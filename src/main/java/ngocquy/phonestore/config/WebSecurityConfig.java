package ngocquy.phonestore.config;

import ngocquy.phonestore.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserDetailsServiceImpl uds;
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(uds).passwordEncoder(passwordEncoder());
    }
    @Override
    protected void configure(HttpSecurity http)throws Exception{
        http
                .authorizeRequests()
                    .antMatchers("/homepage").permitAll()
                    .antMatchers("/login").permitAll()
                    .antMatchers("/register").permitAll()
                    .antMatchers("/admin").hasRole("ADMIN")
                    .antMatchers("/manageOrders").hasRole("ADMIN")
                    .antMatchers("/manageUsers").hasRole("ADMIN")
                    .and()
                    .rememberMe().key("uniqueAndSecret").tokenValiditySeconds(24*60*60)
                    .and()
                .formLogin()
                    .loginPage("/homepage")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/homepage")
                    .failureUrl("/")
                    .and()
                .exceptionHandling()
                    .accessDeniedPage("/404");
    }
}
