//package ngocquy.phonestore.config;
//import java.util.HashSet;
//
//import ngocquy.phonestore.entities.Userc;
//import ngocquy.phonestore.service.UsercServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//
//@Component
//public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
//
//    @Autowired
//    private UsercServiceImpl usi;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Override
//    public void onApplicationEvent(ContextRefreshedEvent arg0) {
//        Userc admin = new Userc();
//        admin.setId(1);
//        admin.setUsername("admin");
//        admin.setEmail("admin@gmail.com");
//        admin.setPassword(passwordEncoder.encode("123456"));
//        admin.setRole("ROLE_ADMIN");
//        usi.save(admin);
//
//        // Member account
//        Userc user = new Userc();
//        user.setId(2);
//        user.setUsername("taonghia");
//        user.setEmail("member@gmail.com");
//        user.setPassword(passwordEncoder.encode("123456"));
//        user.setSdt("033688696");
//        user.setRole("ROLE_MEMBER");
//        user.setAddress("263 Giải Phóng, Hoàng Mai, Hà Nội");
//        usi.save(user);
//    }
//
//}