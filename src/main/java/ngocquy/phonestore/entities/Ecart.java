package ngocquy.phonestore.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ecart database table.
 * 
 */
@Entity
@NamedQuery(name="Ecart.findAll", query="SELECT e FROM Ecart e")
public class Ecart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int price;

	private int quantity;

	//bi-directional many-to-one association to Phone
	@ManyToOne
	private Phone phone;

	//bi-directional many-to-one association to Cart
	@ManyToOne
	private Cart cart;

	public Ecart() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Phone getPhone() {
		return this.phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Cart getCart() {
		return this.cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

}