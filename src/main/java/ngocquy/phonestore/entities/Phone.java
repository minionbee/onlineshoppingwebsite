package ngocquy.phonestore.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the phone database table.
 * 
 */
@Entity
@NamedQuery(name="Phone.findAll", query="SELECT p FROM Phone p")
public class Phone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String description;

	private String name;

	private int price;

	//bi-directional many-to-one association to Ecart
	@OneToMany(mappedBy="phone")
	private List<Ecart> ecarts;

	//bi-directional many-to-one association to Image
	@OneToMany(mappedBy="phone")
	private List<Image> images;

	//bi-directional many-to-one association to Producer
	@ManyToOne
	private Producer producer;

	public Phone() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return this.price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public List<Ecart> getEcarts() {
		return this.ecarts;
	}

	public void setEcarts(List<Ecart> ecarts) {
		this.ecarts = ecarts;
	}

	public Ecart addEcart(Ecart ecart) {
		getEcarts().add(ecart);
		ecart.setPhone(this);

		return ecart;
	}

	public Ecart removeEcart(Ecart ecart) {
		getEcarts().remove(ecart);
		ecart.setPhone(null);

		return ecart;
	}

	public List<Image> getImages() {
		return this.images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Image addImage(Image image) {
		getImages().add(image);
		image.setPhone(this);

		return image;
	}

	public Image removeImage(Image image) {
		getImages().remove(image);
		image.setPhone(null);

		return image;
	}

	public Producer getProducer() {
		return this.producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

}