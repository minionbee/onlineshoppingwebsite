package ngocquy.phonestore.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the orderc database table.
 * 
 */
@Entity
@NamedQuery(name="Orderc.findAll", query="SELECT o FROM Orderc o")
public class Orderc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String address;

	private String amount;

	@Column(name="date_gen")
	private Timestamp dateGen;

	private String payment;

	private int status;
	private String cus;
	private String sdt;
	//bi-directional many-to-one association to Cart
//	@ManyToOne
//	private Cart cart;
	private int cart;
	public Orderc() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Timestamp getDateGen() {
		return this.dateGen;
	}

	public void setDateGen(Timestamp dateGen) {
		this.dateGen = dateGen;
	}

	public String getPayment() {
		return this.payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCart() {
		return cart;
	}

	public void setCart(int cart) {
		this.cart = cart;
	}

	public String getCus() {
		return cus;
	}

	public void setCus(String cus) {
		this.cus = cus;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}
	//	public Cart getCart() {
//		return this.cart;
//	}
//
//	public void setCart(Cart cart) {
//		this.cart = cart;
//	}

}