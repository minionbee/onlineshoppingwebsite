package ngocquy.phonestore.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the userc database table.
 * 
 */
@Entity
@NamedQuery(name="Userc.findAll", query="SELECT u FROM Userc u")
public class Userc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String email;

	private String name;

	private String password;

	private String role;

	private String username;

	private String address;
	private String sdt;
	private String authen;
	//bi-directional many-to-one association to Cart
	@OneToMany(mappedBy="userc")
	private List<Cart> carts;

	public Userc() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Cart> getCarts() {
		return this.carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public String getAuthen() {
		return authen;
	}

	public void setAuthen(String authen) {
		this.authen = authen;
	}

	public Cart addCart(Cart cart) {
		getCarts().add(cart);
		cart.setUserc(this);

		return cart;
	}

	public Cart removeCart(Cart cart) {
		getCarts().remove(cart);
		cart.setUserc(null);

		return cart;
	}

}