package ngocquy.phonestore.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cart database table.
 * 
 */
@Entity
@NamedQuery(name="Cart.findAll", query="SELECT c FROM Cart c")
public class Cart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="cookie_code")
	private String cookieCode;

	//bi-directional many-to-one association to Userc
	@ManyToOne
	private Userc userc;
	private String cpl;
	//bi-directional many-to-one association to Ecart
	@OneToMany(mappedBy="cart")
	private List<Ecart> ecarts;

	//bi-directional many-to-one association to Orderc
//	@OneToMany(mappedBy="cart")
//	private List<Orderc> ordercs;

	public Cart() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCookieCode() {
		return this.cookieCode;
	}

	public void setCookieCode(String cookieCode) {
		this.cookieCode = cookieCode;
	}

	public Userc getUserc() {
		return this.userc;
	}

	public void setUserc(Userc userc) {
		this.userc = userc;
	}

	public List<Ecart> getEcarts() {
		return this.ecarts;
	}

	public void setEcarts(List<Ecart> ecarts) {
		this.ecarts = ecarts;
	}

	public Ecart addEcart(Ecart ecart) {
		getEcarts().add(ecart);
		ecart.setCart(this);

		return ecart;
	}

	public Ecart removeEcart(Ecart ecart) {
		getEcarts().remove(ecart);
		ecart.setCart(null);

		return ecart;
	}

	public String getCpl() {
		return cpl;
	}

	public void setCpl(String cpl) {
		this.cpl = cpl;
	}
	//	public List<Orderc> getOrdercs() {
//		return this.ordercs;
//	}
//
//	public void setOrdercs(List<Orderc> ordercs) {
//		this.ordercs = ordercs;
//	}
//
//	public Orderc addOrderc(Orderc orderc) {
//		getOrdercs().add(orderc);
//		orderc.setCart(this);
//
//		return orderc;
//	}
//
//	public Orderc removeOrderc(Orderc orderc) {
//		getOrdercs().remove(orderc);
//		orderc.setCart(null);
//
//		return orderc;
//	}

}