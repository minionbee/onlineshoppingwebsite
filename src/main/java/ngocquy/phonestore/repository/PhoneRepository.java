package ngocquy.phonestore.repository;

import ngocquy.phonestore.entities.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface PhoneRepository  extends JpaRepository<Phone,Long> {
    List<Phone> findAllByProducerNameOrderByPriceDesc(String name);
    List<Phone> findAllByProducerNameOrderByPriceAsc(String name);
    List<Phone> findAllByOrderByPriceDesc();
    List<Phone> findAllByOrderByPriceAsc();
    List<Phone> findAllByNameContainingOrderByPriceDesc(String name);
    List<Phone> findAllByNameContainingOrderByPriceAsc(String name);
    List<Phone> findAllByPriceBetweenOrderByPriceDesc(int x,int y);
    List<Phone> findAllByPriceGreaterThanOrderByPriceDesc(int x);
    List<Phone> findAllByPriceBetweenOrderByPriceAsc(int x,int y);
    List<Phone> findAllByPriceGreaterThanOrderByPriceAsc(int x);
    Phone findById(int id);
    Phone findByName(String name);
}
