package ngocquy.phonestore.repository;

import ngocquy.phonestore.entities.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepository extends JpaRepository<Producer,Long> {
    Producer findById(int id);
    Producer findByName(String name);
}
