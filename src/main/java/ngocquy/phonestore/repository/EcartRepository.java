package ngocquy.phonestore.repository;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Ecart;
import ngocquy.phonestore.entities.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EcartRepository extends JpaRepository<Ecart,Long> {
    List<Ecart> findAllByCart(Cart cart);
    Ecart findByPhoneAndAndCart(Phone phone,Cart cart);
}
