package ngocquy.phonestore.repository;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Userc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long> {
    Cart findByUserc(Userc userc);
    Cart findById(int id);
    Cart findByUsercName(String name);
    Cart findByCplAndUserc(String cpl,Userc user);
}
