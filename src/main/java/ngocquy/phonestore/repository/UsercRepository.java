package ngocquy.phonestore.repository;
import ngocquy.phonestore.entities.Userc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsercRepository extends JpaRepository<Userc,Long> {
    Userc findUserByUsername(String username);
    Userc findByAuthen(String authen);
}
