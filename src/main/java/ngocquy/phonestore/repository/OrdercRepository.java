package ngocquy.phonestore.repository;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Orderc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdercRepository extends JpaRepository<Orderc,Long> {
    Orderc findByCart(int cart);
    Orderc findById(int id);
}
