package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Orderc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrdercService {
    void save(Orderc o);
    void delete(Orderc o);
    Orderc findByCart(int cart);
    Orderc findById(int id);
    List<Orderc> findAll();
    Page<Orderc> findPaginatedOrder(Pageable pageable,List<Orderc> ordercs);
}
