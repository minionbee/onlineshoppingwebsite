package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Userc;

import java.util.List;

public interface CartService {
    Cart findByUser(Userc user);
    Cart findByUserAndNotCompleted(String cpl,Userc user);
    void save(Cart cart);
    void delete(Cart cart);
    //void update(Cart cart);
    Cart findById(int id);
    List<Cart> findAll();
    Cart findByUserName(String name);
}
