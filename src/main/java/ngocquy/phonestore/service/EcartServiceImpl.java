package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Ecart;
import ngocquy.phonestore.entities.Phone;
import ngocquy.phonestore.repository.EcartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EcartServiceImpl implements EcartService
{
    @Autowired
    private EcartRepository er;

    @Override
    public List<Ecart> findAllByCart(Cart cart) {
        return er.findAllByCart(cart);
    }

    @Override
    public void save(Ecart c) {
        er.save(c);
    }

    @Override
    public void delete(Ecart c) {
        er.delete(c);
    }

    @Override
    public Ecart findByPhoneAndCart(Phone p, Cart cart) {
        return er.findByPhoneAndAndCart(p, cart);
    }


    @Override
    public List<Ecart> findAll() {
        return er.findAll();
    }
}
