package ngocquy.phonestore.service;



import ngocquy.phonestore.entities.Producer;

import java.util.List;

public interface ProducerService {
    Producer findById(int id);
    List<Producer> findAll();
    Producer findByName(String name);
}
