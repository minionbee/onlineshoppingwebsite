package ngocquy.phonestore.service;


import ngocquy.phonestore.entities.Image;
import ngocquy.phonestore.entities.Phone;

import java.util.List;

public interface ImageService {
    List<Image> findAllImage(Phone phone);
    void save(Image image);
    void delete(Image image);
    List<Image> findAll();
}
