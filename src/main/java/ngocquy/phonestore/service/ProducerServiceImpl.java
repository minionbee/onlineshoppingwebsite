package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Producer;
import ngocquy.phonestore.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProducerServiceImpl implements ProducerService {
    @Autowired
    private ProducerRepository pr;
    @Override
    public Producer findById(int id) {
        return pr.findById(id);
    }

    @Override
    public List<Producer> findAll() {
        return pr.findAll();
    }

    @Override
    public Producer findByName(String name) {
        return pr.findByName(name);
    }
}
