package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Orderc;
import ngocquy.phonestore.repository.OrdercRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrdercServiceImpl implements OrdercService {
    @Autowired
    private OrdercRepository or;
    @Override
    public void save(Orderc o) {
        or.save(o);
    }

    @Override
    public void delete(Orderc o) {
        or.delete(o);
    }

    @Override
    public Orderc findByCart(int cart) {
        return or.findByCart(cart);
    }

    @Override
    public Orderc findById(int id) {
        return or.findById(id);
    }

    @Override
    public List<Orderc> findAll() {
        return or.findAll();
    }

    @Override
    public Page<Orderc> findPaginatedOrder(Pageable pageable, List<Orderc> ordercs) {
        int currentPage=pageable.getPageNumber();
        int pageSize=pageable.getPageSize();
        int startItem=currentPage*pageSize;
        List<Orderc> list;
        int toIndex=Math.min(startItem+pageSize,ordercs.size());
        list=ordercs.subList(startItem,toIndex);
        Page<Orderc> page=new PageImpl<>(list, PageRequest.of(currentPage,pageSize),ordercs.size());
        return page;
    }
}
