package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Userc;
import ngocquy.phonestore.repository.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements CartService{
    @Autowired
    private CartRepository cr;
    @Override
    public Cart findByUser(Userc user) {
        return cr.findByUserc(user);
    }

    @Override
    public Cart findByUserAndNotCompleted(String cpl, Userc user) {
        return cr.findByCplAndUserc(cpl,user);
    }

    @Override
    public void save(Cart cart) {
        cr.save(cart);
    }

    @Override
    public void delete(Cart cart) {
        cr.delete(cart);
    }

    @Override
    public Cart findById(int id) {
        return cr.findById(id);
    }

    @Override
    public List<Cart> findAll() {
        return cr.findAll();
    }

    @Override
    public Cart findByUserName(String name) {
        return cr.findByUsercName(name);
    }
}
