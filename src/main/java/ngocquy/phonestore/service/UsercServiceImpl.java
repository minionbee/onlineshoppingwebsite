package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Userc;
import ngocquy.phonestore.repository.UsercRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsercServiceImpl implements UsercService {
    @Autowired
    private UsercRepository usercRepository;

    @Override
    public List<Userc> findAll() {
        return usercRepository.findAll();
    }

    @Override
    public Userc findOne(String username) {
        return usercRepository.findUserByUsername(username);
    }

    @Override
    public void save(Userc user) {
        usercRepository.save(user);
    }

    @Override
    public void delete(Userc user) {
        usercRepository.delete(user);
    }

    @Override
    public Userc findAnonymous(String id_anonymous) {
        return usercRepository.findByAuthen(id_anonymous);
    }

    @Override
    public Page<Userc> findPaginationUser(Pageable pageable, List<Userc> usercs) {
        int currentPage=pageable.getPageNumber();
        int sizePage=pageable.getPageSize();
        int startItem=currentPage*sizePage;
        List<Userc> list;
        int toIndex=Math.min(startItem+sizePage,usercs.size());
        list=usercs.subList(startItem,toIndex);
        Page<Userc> page=new PageImpl<>(list, PageRequest.of(currentPage,sizePage),usercs.size());
        return page;

    }
}
