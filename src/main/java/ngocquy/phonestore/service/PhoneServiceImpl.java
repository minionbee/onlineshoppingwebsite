package ngocquy.phonestore.service;
import ngocquy.phonestore.entities.Phone;
import ngocquy.phonestore.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class PhoneServiceImpl implements PhoneService
{
    @Autowired
    private PhoneRepository pr;


    @Override
    public List<Phone> findAllByNameContainingDesc(String name) {
        return pr.findAllByNameContainingOrderByPriceDesc(name);
    }

    @Override
    public List<Phone> findAllByNameContainingAsc(String name) {
        return pr.findAllByNameContainingOrderByPriceAsc(name);
    }

    @Override
    public List<Phone> findAllByProducerDesc(String producer_name) {
        return pr.findAllByProducerNameOrderByPriceDesc(producer_name);
    }

    @Override
    public List<Phone> findAllByProducerAsc(String producer_name) {
        return pr.findAllByProducerNameOrderByPriceAsc(producer_name);
    }

    @Override
    public void save(Phone phone) {
        pr.save(phone);
    }

    @Override
    public void delete(Phone phone) {
        pr.delete(phone);
    }

    @Override
    public List<Phone> findAllOrderByPriceDesc() {
        return pr.findAllByOrderByPriceDesc();
    }

    @Override
    public List<Phone> findAllOrderByPriceAsc() {
        return pr.findAllByOrderByPriceAsc();
    }

    @Override
    public List<Phone> findAllByPriceBetweenAndOrderDesc(int x, int y) {
        return pr.findAllByPriceBetweenOrderByPriceDesc(x,y);
    }

    @Override
    public List<Phone> findAllByPriceBetweenAndOrderAsc(int x, int y) {
        return pr.findAllByPriceBetweenOrderByPriceAsc(x,y);
    }

    @Override
    public List<Phone> findAllByPriceGreaterThanOrderDesc(int x) {
        return pr.findAllByPriceGreaterThanOrderByPriceDesc(x);
    }

    @Override
    public List<Phone> findAllByPriceGreaterThanOrderAsc(int x) {
        return pr.findAllByPriceGreaterThanOrderByPriceAsc(x);
    }

    @Override
    public Phone findById(int id) {
        return pr.findById(id);
    }

    @Override
    public Phone findByName(String name) {
        return pr.findByName(name);
    }

    @Override
    public Page<Phone> findPaginated(Pageable pageable,List<Phone> phones) {
        int pageSize=pageable.getPageSize();
        int currentPage=pageable.getPageNumber();
        int startItem=currentPage*pageSize;
        List<Phone> list;
        if(phones.size()<startItem){
            list= Collections.emptyList();
        }
        else{
            int toIndex=Math.min(startItem+pageSize,phones.size());
            list=phones.subList(startItem,toIndex);
        }
        Page<Phone> phonePage=new PageImpl<>(list, PageRequest.of(currentPage,pageSize),phones.size());
        return phonePage;
    }

    @Override
    public List<Phone> findAll() {
        return pr.findAll();
    }
}
