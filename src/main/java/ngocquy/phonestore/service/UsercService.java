package ngocquy.phonestore.service;


import ngocquy.phonestore.entities.Userc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UsercService {
    List<Userc> findAll();
    Userc findOne(String username);
    void save(Userc user);
    void delete(Userc user);
    Userc findAnonymous(String id);
    Page<Userc> findPaginationUser(Pageable pageable,List<Userc> usercs);
}
