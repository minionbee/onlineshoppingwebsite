package ngocquy.phonestore.service;


import ngocquy.phonestore.entities.Phone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PhoneService {
    List<Phone> findAllByNameContainingDesc(String name);
    List<Phone> findAllByNameContainingAsc(String name);
    List<Phone> findAllByProducerDesc(String producer_name);
    List<Phone> findAllByProducerAsc(String producer_name);
    void save(Phone phone);
    void delete(Phone phone);
    List<Phone> findAllOrderByPriceDesc();
    List<Phone> findAllOrderByPriceAsc();
    List<Phone> findAllByPriceBetweenAndOrderDesc(int x,int y);
    List<Phone> findAllByPriceBetweenAndOrderAsc(int x,int y);
    List<Phone> findAllByPriceGreaterThanOrderDesc(int x);
    List<Phone> findAllByPriceGreaterThanOrderAsc(int x);
    Phone findById(int id);
    Phone findByName(String name);
    Page<Phone> findPaginated(Pageable pageable,List<Phone> phones);
    List<Phone> findAll();
}
