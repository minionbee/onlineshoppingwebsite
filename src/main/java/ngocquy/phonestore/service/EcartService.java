package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Cart;
import ngocquy.phonestore.entities.Ecart;
import ngocquy.phonestore.entities.Phone;

import java.util.List;

public interface EcartService {
    List<Ecart> findAllByCart(Cart cart);
    void save(Ecart c);
    void delete(Ecart c);
    Ecart findByPhoneAndCart(Phone p,Cart cart);
    List<Ecart> findAll();
}
