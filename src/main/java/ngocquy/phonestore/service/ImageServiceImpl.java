package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Image;
import ngocquy.phonestore.entities.Phone;
import ngocquy.phonestore.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService{
    @Autowired
    private ImageRepository ir;
    @Override
    public List<Image> findAllImage(Phone phone) {
        return ir.findAllByPhone(phone);
    }

    @Override
    public void save(Image image) {
        ir.save(image);
    }

    @Override
    public void delete(Image image) {
        ir.delete(image);
    }


    @Override
    public List<Image> findAll() {
        return ir.findAll();
    }
}
