package ngocquy.phonestore.service;

import ngocquy.phonestore.entities.Userc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UsercServiceImpl usi;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Userc user=this.usi.findOne(username);
        if(user==null){
            System.out.println("User: "+username+" not found!");
            throw new UsernameNotFoundException("User: "+username+" not found!");
        }
        List<GrantedAuthority> ga=new ArrayList<>();
        ga.add(new SimpleGrantedAuthority(user.getRole()));
        UserDetails userDetails=new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),ga);
        return userDetails;
    }
}