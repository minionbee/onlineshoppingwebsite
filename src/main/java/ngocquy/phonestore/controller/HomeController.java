package ngocquy.phonestore.controller;

import ngocquy.phonestore.entities.*;
import ngocquy.phonestore.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class HomeController {
    @Autowired
    private UsercServiceImpl userService;
    @Autowired
    private PhoneServiceImpl phoneService;
    @Autowired
    private ImageServiceImpl imageService;
    @Autowired
    private ProducerServiceImpl producerService;
    @GetMapping(value = {"/homepage","/"})
    public String homePage(Model model){
        model.addAttribute("listPhone",phoneService.findAll());
        return "homepage";
    }
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @PostMapping("/register")
    public String register(HttpServletRequest request,Model model){
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String email=request.getParameter("email");
        String fullname=request.getParameter("fullname");
        String sdt=request.getParameter("sdt");
        String address=request.getParameter("address");
        Userc user=new Userc();
        user.setId(userService.findAll().size()+1);
        BCryptPasswordEncoder b=new BCryptPasswordEncoder();
        user.setUsername(username);user.setPassword(b.encode(password));user.setRole("ROLE_MEMBER");user.setEmail(email);
        user.setSdt(sdt);user.setName(fullname);user.setAddress(address);
        userService.save(user);
        model.addAttribute("listPhone",phoneService.findAll());
        return "homepage";
    }
    @GetMapping("/404")
    public String error(){
        return "404";
    }

    // product page
    @GetMapping("/product")
    public String displayPhone(HttpServletRequest request,Model model,
                               @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllOrderByPriceDesc(),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllOrderByPriceAsc(),currentPage,pageSize,1);
        return "product";
    }
    @GetMapping("/product/{producer}")
    public String displayProductByProducer(HttpServletRequest request,@PathVariable("producer")String name,Model model,
                                           @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByProducerDesc(name),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByProducerAsc(name),currentPage,pageSize,1);
        model.addAttribute("key",name);
        return "product";
    }
    @GetMapping("/product/duoi-2-trieu")
    public String displayProduct1(HttpServletRequest request,Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByPriceBetweenAndOrderDesc(0,2000000),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByPriceBetweenAndOrderAsc(0,2000000),currentPage,pageSize,1);
        model.addAttribute("key","duoi-2-trieu");
        return "product";
    }
    @GetMapping("/product/tu-2-5-trieu")
    public String displayProduct2(HttpServletRequest request,Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByPriceBetweenAndOrderDesc(2000000,5000000),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByPriceBetweenAndOrderAsc(2000000,5000000),currentPage,pageSize,1);
        model.addAttribute("key","tu-2-5-trieu");
        return "product";
    }
    @GetMapping("/product/tu-5-10-trieu")
    public String displayProduct3(HttpServletRequest request,Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByPriceBetweenAndOrderDesc(5000000,10000000),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByPriceBetweenAndOrderAsc(5000000,10000000),currentPage,pageSize,1);
        model.addAttribute("key","tu-5-10-trieu");
        return "product";
    }
    @GetMapping("/product/tren-10-trieu")
    public String displayProduct4(HttpServletRequest request,Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByPriceGreaterThanOrderDesc(10000000),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByPriceGreaterThanOrderDesc(10000000),currentPage,pageSize,1);
        model.addAttribute("key","tren-10-trieu");
        return "product";
    }

    // view detail a smartphone
    @PostMapping("/product/{name}")
    public String detailPhone(@PathVariable("name")String phone_name,Model model){
        model.addAttribute("phone",phoneService.findByName(phone_name));
        return "detailPhone";
    }
    // search a smartphone
    @GetMapping("/product/search")
    public String searchPhone(HttpServletRequest request,Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);
        String phone_name=request.getParameter("phone_name");
        if(request.getParameter("sort")==null||request.getParameter("sort").equals("0"))
            findPage(model,phoneService.findAllByNameContainingDesc(phone_name),currentPage,pageSize,0);
        else findPage(model,phoneService.findAllByNameContainingAsc(phone_name),currentPage,pageSize,1);
        model.addAttribute("key","search");
        model.addAttribute("pName",phone_name);
        return "product";
    }
    public void findPage(Model model,List<Phone> phones,int currentPage,int pageSize,int sort){
        HashMap<String,Integer> searchByProducers=new HashMap<>();
        List<Producer> producers=producerService.findAll();
        for(int i=0;i<producers.size();i++)
        {
            List<Phone> list=phoneService.findAllByProducerDesc(producers.get(i).getName());
            searchByProducers.put(producers.get(i).getName(),list.size());
        }
        model.addAttribute("searchByProducers",searchByProducers);
        //
        Page<Phone> phonePage = phoneService.findPaginated(PageRequest.of(currentPage - 1, pageSize),phones);

        model.addAttribute("phonePage", phonePage);

        int totalPages = phonePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        if(sort==0)model.addAttribute("order_sort",0);
        else model.addAttribute("order_sort",1);
    }
}
