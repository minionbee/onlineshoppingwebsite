package ngocquy.phonestore.controller.customer;

import ngocquy.phonestore.entities.*;
import ngocquy.phonestore.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Controller
public class CustomerController {
    @Autowired
    private UsercService userService;
    @Autowired
    private PhoneService phoneService;
    @Autowired
    private CartService cartService;
    @Autowired
    private OrdercService orderService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private ProducerService producerService;
    @Autowired
    private EcartService ecartService;
    // view cart page
    @GetMapping("/{user}/cart")
    public String getCart(HttpServletRequest request, @PathVariable("user")String username, Model model){
        if(username.equals("anonymous")){
            System.out.println(request.getSession().getId());
            if(userService.findAnonymous(request.getSession().getId())==null){
                Userc user=createNewAnonymousUser(request.getSession().getId());
                userService.save(user);
                cartService.save(createNewCart(user));
            }
            else {
                Userc user = userService.findAnonymous(request.getSession().getId());
                if (cartService.findByUserAndNotCompleted("not complete", user) == null)
                    cartService.save(createNewCart(user));
            }
            model.addAttribute("cart", cartService.findByUserAndNotCompleted("not complete",userService.findAnonymous(request.getSession().getId())));
        }
        else{
            Userc user=userService.findOne(username);
            if(cartService.findByUserAndNotCompleted("not complete",user)==null)
                cartService.save(createNewCart(user));
            model.addAttribute("cart", cartService.findByUserAndNotCompleted("not complete",user));
        }
        return "cart";
    }
    // add a item to cart
    @PostMapping("/{user}/cart")
    public String cart(@PathVariable("user")String username, Model model,HttpServletRequest request){
        // anonymous_id user
        String anonymous_id=request.getSession().getId();
        // phone add to cart
        Phone phone=phoneService.findById(Integer.parseInt(request.getParameter("phone")));
        int price=phone.getPrice();
        // anonymous user
        if(username.equals("anonymous")){
            // anonymous user has a account
            if(userService.findAnonymous(anonymous_id)!=null){
                if(cartService.findByUserAndNotCompleted("not complete",userService.findAnonymous(anonymous_id))!=null){
                    Cart cart=cartService.findByUserAndNotCompleted("not complete",userService.findAnonymous(anonymous_id));
                    if(ecartService.findByPhoneAndCart(phone,cart)==null){
                        ecartService.save(createNewEcart(cart,phone, price));
                    }
                    else{
                        updateEcart(ecartService.findByPhoneAndCart(phone,cart));
                    }
                }
                else if(cartService.findByUserAndNotCompleted("not complete",userService.findAnonymous(anonymous_id))==null){
                    Cart cart=createNewCart(userService.findAnonymous(anonymous_id));
                    cartService.save(cart);
                    ecartService.save(createNewEcart(cart,phone, price));
                }
            }
            // anonymous user hasn't a account
            else{
                Userc user=createNewAnonymousUser(anonymous_id);
                if(cartService.findByUserAndNotCompleted("not complete",user)!=null){
                    Cart cart=cartService.findByUserAndNotCompleted("not complete",user);
                    if(ecartService.findByPhoneAndCart(phone,cart)==null)
                        ecartService.save(createNewEcart(cart,phone, price));
                    else{
                        updateEcart(ecartService.findByPhoneAndCart(phone,cart));
                    }
                }
                else if(cartService.findByUserAndNotCompleted("not complete",user)==null){
                    Cart cart=createNewCart(user);
                    cartService.save(cart);
                    ecartService.save(createNewEcart(cart,phone,price));
                }
            }
        }
        // member
        else{
            if(cartService.findByUserAndNotCompleted("not complete",userService.findOne(username))!=null){
                Userc user=userService.findOne(username);
                Cart cart=cartService.findByUserAndNotCompleted("not complete",user);
                if(ecartService.findByPhoneAndCart(phone,cart)==null){
                    ecartService.save(createNewEcart(cart,phone,price));
                }
                else{
                    updateEcart(ecartService.findByPhoneAndCart(phone,cart));
                }
            }
            else if(cartService.findByUserAndNotCompleted("not complete",userService.findOne(username))==null){
                Cart cart=createNewCart(userService.findOne(username));
                cartService.save(cart);
                ecartService.save(createNewEcart(cart,phone,price));
            }
        }
        return getCart(request,username,model);
    }

    // create a order and send order to ebee store
    @PostMapping("/{user}/checkout")
    public String checkout(@PathVariable("user")String username, Model model, HttpServletRequest request) {
        String anonymous_id = request.getSession().getId();
        String[] qty = request.getParameterValues("qty");
        Userc user;
        if(username.equals("anonymous"))user=userService.findAnonymous(anonymous_id);
        else user = userService.findOne(username);
        if (request.getParameter("dathang").equals("0")) {
            Cart cart = cartService.findByUserAndNotCompleted("not complete", user);
            List<Ecart> ecarts = ecartService.findAllByCart(cart);
            int amount1 = updateListEcart(ecarts, qty);
            model.addAttribute("user", user);
            model.addAttribute("cart", cart);
            model.addAttribute("amount", amount1);
            return "checkout";
        } else {
            int id_cart=Integer.parseInt(request.getParameter("id_cart"));
            String address_user=request.getParameter("address_user");
            String amount=request.getParameter("price_cart");
            String payment=request.getParameter("payment");
            String name_user=request.getParameter("name_user");
            String phone_user=request.getParameter("phone_user");
            if (orderService.findByCart(Integer.parseInt(request.getParameter("id_cart"))) == null &&
                    cartService.findById(id_cart).getCpl().equals("not complete")) {
                orderService.save(createNewOrder(id_cart, address_user, amount, payment, name_user, phone_user));
            } else {
                Orderc order = orderService.findByCart(Integer.parseInt(request.getParameter("id_cart")));
                orderService.save(updateOrder(order, amount));
            }
        }
        model.addAttribute("listPhone",phoneService.findAll());
        return "homepage";
    }
    // update ecart when changing quantity item in cart view and get amount
    public int updateListEcart(List<Ecart> ecarts,String[] qty){
        int amount = 0;
        for (int i = 0; i < ecarts.size(); i++) {
            ecarts.get(i).setQuantity(Integer.parseInt(qty[i]));
            if(ecarts.get(i).getQuantity()!=0){
                ecartService.save(ecarts.get(i));
                amount += ecarts.get(i).getPrice() * Integer.parseInt(qty[i]);
            }
            else {
                ecartService.delete(ecarts.get(i));
                ecarts.remove(i);
            }
        }
        return amount;
    }
    // create new order
    public Orderc createNewOrder(int id_cart,String address,String amount,String payment,String name,String phone){
        Orderc order=new Orderc();
        order.setId(orderService.findAll().size()+1);
        order.setCart(id_cart);
        order.setAddress(address);
        order.setAmount(amount);
        order.setPayment(payment);
        order.setCus(name);
        order.setSdt(phone);
        order.setStatus(0);
        order.setDateGen(new Timestamp(new Date().getTime()));
        return order;
    }
    // update an exist order
    public Orderc updateOrder(Orderc order,String amount){
        order.setAmount(amount);
        order.setDateGen(new Timestamp(new Date().getTime()));
        return  order;
    }
    // create new cart
    public Cart createNewCart(Userc user){
        Cart cart = new Cart();
        cart.setId(cartService.findAll().size() + 1);
        cart.setUserc(user);
        cart.setCpl("not complete");
        return cart;
    }
    // create new account for anonymous user
    public Userc createNewAnonymousUser(String sessionId){
        Userc user=new Userc();
        user.setId(userService.findAll().size()+1);
        user.setAuthen(sessionId);
        user.setName("Anonymous "+(userService.findAll().size()+1));
        return user;
    }
    // add a smartphone to user's cart
    public Ecart createNewEcart(Cart cart, Phone phone,int price){
        Ecart ecart=new Ecart();
        ecart.setId(ecartService.findAll().size()+1);
        ecart.setPhone(phone);
        ecart.setQuantity(1);
        ecart.setPrice(price);
        ecart.setCart(cart);
        return ecart;
    }
    // update ecart when add a exist phone to cart
    public void updateEcart(Ecart ecart){
        ecart.setQuantity(ecart.getQuantity()+1);
        ecartService.save(ecart);
    }
}
