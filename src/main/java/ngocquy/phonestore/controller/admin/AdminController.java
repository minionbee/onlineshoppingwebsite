package ngocquy.phonestore.controller.admin;

import ngocquy.phonestore.entities.*;
import ngocquy.phonestore.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class AdminController {
    @Autowired
    private PhoneService phoneService;
    @Autowired
    private OrdercService ordercService;
    @Autowired
    private UsercService usercService;
    @Autowired
    private ProducerService producerService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private CartService cartService;
    // phone page
    @GetMapping("/admin/qly-dienthoai")
    public String manageSmartPhone(Model model, @RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(6);

        Page<Phone> phonePage = phoneService.findPaginated(PageRequest.of(currentPage - 1, pageSize),phoneService.findAll());

        model.addAttribute("phonePage", phonePage);

        int totalPages = phonePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "managePhones";
    }

    // user page
    @GetMapping("/admin/qly-khachhang")
    public String manageCustomer(Model model,@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size){
        int currentPage=page.orElse(1);
        int sizePage=size.orElse(10);
        Page<Userc> userPage=usercService.findPaginationUser(PageRequest.of(currentPage-1,sizePage),usercService.findAll());
        model.addAttribute("userPage",userPage);
        int totalPage=userPage.getTotalPages();
        if(totalPage>0){
            List<Integer> pageNumbers=IntStream.rangeClosed(1,totalPage)
                    .boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers",pageNumbers);
        }
        return "manageUsers";
    }

    // order page
    @GetMapping("/admin/qly-donhang")
    public String manageOrders(Model model,@RequestParam("page") Optional<Integer> page,@RequestParam("size") Optional<Integer> size){
        int currentPage=page.orElse(1);
        int sizePage=size.orElse(10);
        Page<Orderc> orderPage=ordercService.findPaginatedOrder(PageRequest.of(currentPage-1,sizePage),ordercService.findAll());
        model.addAttribute("orderPage",orderPage);
        int totalPage=orderPage.getTotalPages();
        if(totalPage>0){
            List<Integer> pageNumbers=IntStream.rangeClosed(1,totalPage)
                    .boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers",pageNumbers);
        }
        return "manageOrders";
    }

    // confirm an order
    @GetMapping("/admin/qly-donhang/{id}")
    public String confirmAnOrder(@PathVariable("id")int id,Model model){
        Orderc order=ordercService.findById(id);
        order.setStatus(1);
        ordercService.save(order);
        Cart cart=cartService.findById(order.getCart());
        cart.setCpl("completed");
        cartService.save(cart);
        model.addAttribute("orders",ordercService.findAll());
        return manageOrders(model, Optional.of(1),Optional.of(10));
    }

    // add a smartphone
    @GetMapping("/admin/qly-dienthoai/add")
    public String addPhone(Model model){
        model.addAttribute("producer",producerService.findAll());
        return "addPhone";
    }
    @PostMapping("/admin/qly-dienthoai/add")
    public String confirmAdd(HttpServletRequest request, Model model,@RequestParam("name") String name,
                           @RequestParam("price") String price,@RequestParam("description") String description,@RequestParam("producer") String producer){
        Phone p=new Phone();
        p.setName(name);
        p.setPrice(Integer.parseInt(price));
        p.setDescription(description);
        p.setProducer(producerService.findByName(producer));
        p.setId(phoneService.findAll().size()+1);
        phoneService.save(p);
        List<Image> images=new ArrayList<>();
        String[] urls=request.getParameterValues("images");
        for(int i=0;i<urls.length;i++){
            Image image=new Image();
            image.setPhone(p);
            image.setUrl("/image/"+producer+"/"+urls[i]);
            image.setId(imageService.findAll().size()+1);
            imageService.save(image);
            images.add(image);
        }
        return manageSmartPhone(model,Optional.of(1),Optional.of(6));
    }

    // edit a smartphone
    @GetMapping("/admin/qly-dienthoai/edit/{id}/phone")
    public String editPhone(@PathVariable("id")int id,Model model){
        model.addAttribute("phone",phoneService.findById(id));
        model.addAttribute("producer",producerService.findAll());
        return "editPhone";
    }
    @PostMapping("/admin/qly-dienthoai/edit")
    public String confirmEdit(HttpServletRequest request,Model model,@RequestParam("name") String name,
                            @RequestParam("price") String price,@RequestParam("description") String description,@RequestParam("producer") String producer){
        Phone p=phoneService.findById(Integer.parseInt(request.getParameter("id")));
        p.setName(name);
        p.setPrice(Integer.parseInt(price));
        p.setDescription(description);
        p.setProducer(producerService.findByName(producer));
        phoneService.save(p);
        List<Image> deleteImages=imageService.findAllImage(p);
        for(int i=0;i<deleteImages.size();i++)
            imageService.delete(deleteImages.get(i));
        List<Image> images=new ArrayList<>();
        String[] urls=request.getParameterValues("images");
        for(int i=0;i<urls.length;i++){
            Image image=new Image();
            image.setPhone(p);
            image.setUrl("/image/"+producer+"/"+urls[i]);
            image.setId(imageService.findAll().size()+1);
            imageService.save(image);
            images.add(image);
        }
        return manageSmartPhone(model,Optional.of(1),Optional.of(6));
    }

    // delete a smartphone
    @GetMapping("/admin/qly-dienthoai/delete/{id}/phone")
    public String deletePhone(@PathVariable("id")int id, Model model){
        Phone p=phoneService.findById(id);
        List<Image> images=imageService.findAllImage(p);
        for(int i=0;i<images.size();i++)
            imageService.delete(images.get(i));
        phoneService.delete(p);
        return manageSmartPhone(model,Optional.of(1),Optional.of(6));
    }
}
