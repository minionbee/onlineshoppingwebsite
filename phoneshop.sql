CREATE TABLE Userc (id int(10) NOT NULL AUTO_INCREMENT, username varchar(255), password varchar(255), email varchar(255), role varchar(20), name varchar(255), PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Image (id int(10) NOT NULL AUTO_INCREMENT, url varchar(255), phone_id int(10) NOT NULL, PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Phone (id int(10) NOT NULL AUTO_INCREMENT, name varchar(100), price int(10), description varchar(255), producer_id int(11) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Orderc (id int(10) NOT NULL AUTO_INCREMENT, address varchar(255), payment varchar(255), status int(10), date_gen timestamp NULL, amount varchar(255), cart_id int(10) NOT NULL UNIQUE, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Ecart (id int(10) NOT NULL AUTO_INCREMENT, price int(10), quantity int(10), phone_id int(10) NOT NULL, cart_id int(10) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Cart (id int(10) NOT NULL AUTO_INCREMENT, cookie_code varchar(255), userc_id int(10) NOT NULL UNIQUE, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE Producer (id int(11) NOT NULL AUTO_INCREMENT, name varchar(255), hotline varchar(12), PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE Ecart ADD CONSTRAINT FKEcart247702 FOREIGN KEY (phone_id) REFERENCES Phone (id);
ALTER TABLE Image ADD CONSTRAINT FKImage256060 FOREIGN KEY (phone_id) REFERENCES Phone (id);
ALTER TABLE Ecart ADD CONSTRAINT FKEcart276617 FOREIGN KEY (cart_id) REFERENCES Cart (id);
ALTER TABLE Phone ADD CONSTRAINT FKPhone345227 FOREIGN KEY (producer_id) REFERENCES Producer (id);
ALTER TABLE Orderc ADD CONSTRAINT FKOrderc397678 FOREIGN KEY (cart_id) REFERENCES Cart (id);
ALTER TABLE Cart ADD CONSTRAINT FKCart921216 FOREIGN KEY (userc_id) REFERENCES Userc (id);
insert into producer(name,hotline)
values('Apple','+34009800288');
insert into producer(name,hotline)
values('Samsung','+56233688920');
insert into producer(name,hotline)
values('Xiaomi','+20356688120');
insert into producer(name,hotline)
values('Nokia','+69134431688');
insert into producer(name,hotline)
values('Vin smart','+8468868888');
-- phone-- 
insert into phone(name,price,description,producer_id)
values('Iphone 11',22000000,'RAM: 4GB, Bộ nhớ trong: 64GB, Camera trước: 12 MP, Hệ điều hành: iOS 13, Dung lượng pin: 3969 mAh',1);
insert into phone(name,price,description,producer_id)
values('Iphone XS MAX',34000000,'RAM: 4GB, Bộ nhớ trong: 64GB,Hệ điều hành:	iOS 12, Dung lượng pin:	3174 mAh',1);
insert into phone(name,price,description,producer_id)
values('Iphone 8 plus',12500000,'RAM: 3GB,CPU: Apple A11 Bionic 6 nhân, Bộ nhớ trong: 64GB, Hệ điều hành: iOS 12, Dung lượng pin: 2691 mAh',1);
insert into phone(name,price,description,producer_id)
values('Samsung A7',6900000,'RAM: 4GB, Bộ nhớ trong: 64GB, CPU:	Exynos 7885 8 nhân 64-bit, Hệ điều hành: Android 8.0 (Oreo), Camera trước: 24 MP',2);
insert into phone(name,price,description,producer_id)
values('Samsung s10 plus',18900000,'RAM: 8GB, Bộ nhớ trong:	128GB, Camera trước: Chính 10 MP & Phụ 8 MP, Hệ điều hành: Android 9.0 (Pie), Dung lượng pin: 4100 mAh',2);
insert into phone(name,price,description,producer_id)
values('Samsung swift',5100000,'RAM: 4GB, Bộ nhớ trong:	64GB, Camera trước: 8MP, Hệ điều hành: Android 9.0 (Pie), Dung lượng pin: 4000 mAh, CPU: Snapdragon 450 8 nhân',2);
insert into phone(name,price,description,producer_id)
values('Xiaomi note 8',3250000,'RAM: 4GB, Bộ nhớ trong:	64GB, Camera trước:	13 MP, Hệ điều hành: Android 9.0 (Pie), CPU: Qualcomm Snapdragon 665 8 nhân',3);
insert into phone(name,price,description,producer_id)
values('Xiaomi note 9 plus',8900000,'RAM: 6GB, Bộ nhớ trong: 64GB, CPU: Snapdragon 712 8 nhân 64-bit, Camera trước: 20 MP,Hệ điều hành: Android 9.0 (Pie)',3);

-- image--

insert into image(url,phone_id)
values('/image/apple/iphone11_1.png',1);
insert into image(url,phone_id)
values('/image/apple/iphone11_2.png',1);
insert into image(url,phone_id)
values('/image/apple/iphone11_3.png',1);
insert into image(url,phone_id)
values('/image/apple/xsmax_1.png',2);
insert into image(url,phone_id)
values('/image/apple/xsmax_2.png',2);
insert into image(url,phone_id)
values('/image/apple/xsmax_3.png',2);
insert into image(url,phone_id)
values('/image/apple/iphone8_1.jpg',3);
insert into image(url,phone_id)
values('/image/apple/iphone8_2.jpg',3);
insert into image(url,phone_id)
values('/image/apple/iphone8_1.jpg',3);
insert into image(url,phone_id)
values('/image/samsung/s10_1.png',5);
insert into image(url,phone_id)
values('/image/samsung/s10_2.png',5);
insert into image(url,phone_id)
values('/image/samsung/s10_3.png',5);
insert into image(url,phone_id)
values('/image/samsung/a7_1.png',4);
insert into image(url,phone_id)
values('/image/samsung/a7_2.png',4);
insert into image(url,phone_id)
values('/image/samsung/a7_3.png',4);
insert into image(url,phone_id)
values('/image/samsung/sam_1.png',6);
insert into image(url,phone_id)
values('/image/samsung/sam_2.png',6);
insert into image(url,phone_id)
values('/image/samsung/sam_3.png',6);
insert into image(url,phone_id)
values('/image/xiaomi/xi8_1.png',7);
insert into image(url,phone_id)
values('/image/xiaomi/xi8_2.png',7);
insert into image(url,phone_id)
values('/image/xiaomi/xi8_3.png',7);
insert into image(url,phone_id)
values('/image/xiaomi/xi9_1.png',8);
insert into image(url,phone_id)
values('/image/xiaomi/xi9_2.png',8);
insert into image(url,phone_id)
values('/image/xiaomi/xi9_3.png',8);
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/apple/iphone11_1.png', '1');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/apple/xsmax_1.png', '2');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/apple/iphone8_1.jpg', '3');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/samsung/s10_1.png', '5');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/samsung/a7_1.png', '4');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/samsung/sam_1.png', '6');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/xiaomi/xi8_1.png', '7');
INSERT INTO `dbsbee`.`image` (`url`, `phone_id`) VALUES ('/image/xiaomi/xi9_1.png', '8');

